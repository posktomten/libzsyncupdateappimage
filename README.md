# Automated update of AppImage with zsync. Library for applications developed with Qt IDE.

To check for updates, another library is used, [libcheckforupdates](https://gitlab.com/posktomten/libcheckforupdates)<br>

To display the "About" dialog, [libabout](https://gitlab.com/posktomten/libabout) <br>


[Read more!](https://gitlab.com/posktomten/libzsyncupdateappimage/-/wikis/home)
