//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          zsyncUpdateAppImage
//          Copyright (C) 2020 - 2023  Ingemar Ceicer
//          https://gitlab.com/posktomten/zsyncupdateappimage
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QFontDatabase>
#include <QFileIconProvider>
#include <QMovie>

#include "updatedialog.h"
#include "ui_updatedialog.h"

//#include "mainwindow.h"
UpdateDialog::UpdateDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdateDialog)
{
}

void UpdateDialog::viewUpdate(QPixmap *pixmap)

{
    Q_INIT_RESOURCE(resource_libzsynkupdateappimage); // Font from the resource file
    ui->setupUi(this);
    // this->setStyleSheet("background-color: green;");
    // ui->lbl->setStyleSheet("color: black;"
    //                        "background-color: green;"
    //                        "selection-color: yellow;");
    // qDebug() << "UPDATE APPIMAGE " << QApplication::font();
    // Change as desired
    this->setFixedSize(400, 400);
    this->setWindowTitle(tr("Updating..."));
    // QFileIconProvider p;
    // QIcon ico = p.icon(QFileIconProvider::Network);
    // this->setWindowIcon(*icon);
    // QIcon icon(QIcon(":/images/icon.png"));
    QGuiApplication::setWindowIcon(QIcon(*pixmap));
    // this->setWindowFlags(Qt::FramelessWindowHint);
//    this->setStyleSheet("background-color:#daffdd;");
    QString fontPath = ":/fonts/Ubuntu-MediumItalic.ttf";
    int fontId = QFontDatabase::addApplicationFont(fontPath);

    if(fontId != -1) {
        QString family = QFontDatabase::applicationFontFamilies(fontId).at(0);
        QFont f(family);
        f.setPointSize(24);
        f.setWeight(QFont::Normal);
        f.setItalic(true);
        ui->lbl->setFont(f);
    }

//    QMovie *mv = new QMovie("c:/tmp/sun.gif");
//    mv->start();
//    lbl->setAttribute(Qt::WA_NoSystemBackground);
//    lbl->setMovie(mv);
    QMovie* movie = new QMovie(":/images/spinner.gif");

    //    movie->setSpeed(200); // 2x speed
    // Make sure the GIF was loaded correctly
    if(!movie->isValid()) {
        qDebug() << "KASS! " << movie->lastErrorString();
    } else {
//        movie->setScaledSize(QSize(200, 200));
        movie->start();
        ui->lblIcon->setAttribute(Qt::WA_NoSystemBackground);
        ui->lblIcon->setMovie(movie);
    }

    ui->lbl->setAlignment(Qt::AlignCenter);
    ui->lbl->setText(tr("Updating, please wait..."));
}

UpdateDialog::~UpdateDialog()
{
    delete ui;
}
