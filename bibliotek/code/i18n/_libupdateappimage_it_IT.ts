<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="it_IT">
<context>
    <name>Update</name>
    <message>
        <source>Update</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <source>zsync cannot be found in path:
</source>
        <translation>zsync non trovato nel percorso:
</translation>
    </message>
    <message>
        <source>Unable to update.</source>
        <translation>Impossibile effettuare aggiornamento.</translation>
    </message>
    <message>
        <source>An unexpected error occurred. Error message:</source>
        <translation>Si è verificato un errore inaspettato. Messaggio errore:</translation>
    </message>
    <message>
        <source>is updated.</source>
        <translation>è aggiornato.</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <source>Dialog</source>
        <translation>Finestra</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation>EtichettaTesto</translation>
    </message>
    <message>
        <source>Updating...</source>
        <translation>In aggiornamento...</translation>
    </message>
    <message>
        <source>Updating, please wait...</source>
        <translation>Aggiornamento...</translation>
    </message>
</context>
</TS>
