<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Update</name>
    <message>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <source>zsync cannot be found in path:
</source>
        <translation>zsync kan inte hittas i sökvägen:
</translation>
    </message>
    <message>
        <source>Unable to update.</source>
        <translation>Kan inte uppdatera.</translation>
    </message>
    <message>
        <source>An unexpected error occurred. Error message:</source>
        <translation>Ett oväntat fel uppstod. Felmeddelande:</translation>
    </message>
    <message>
        <source>is updated.</source>
        <translation>är uppdaterad.</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <source>Updating...</source>
        <translation>Uppdaterar...</translation>
    </message>
    <message>
        <source>Updating, please wait...</source>
        <translation>Uppdaterar, vänta...</translation>
    </message>
</context>
</TS>
