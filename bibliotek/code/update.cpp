//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          zsyncUpdateAppImage
//          Copyright (C) 2020 - 2023  Ingemar Ceicer
//          https://gitlab.com/posktomten/zsyncupdateappimage
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
#include <QMessageBox>
#include <QPushButton>

#include <QDir>
#include <QProcess>
#include <QCoreApplication>
#include <QDebug>
#include "update.h"

Update::Update(QObject *parent) : QObject(parent)
{
}

// A pointer to QDialog "Updating, please wait ..." so that it can be removed when the update is complete
void Update::doUpdate(const QString zsync_arg_1, const QString zsync_arg_2, const QString progname, QDialog *ud, QPixmap *pixmap)
{
    QStringList arg;
    arg << "-i" << zsync_arg_1 << zsync_arg_2;
    QString path = QCoreApplication::applicationDirPath() + "/zsync";

    if(! fileExists(path)) {  // Check if zsync is available
        delete ud; // QDialog "Updating, please wait ..." is deletet
        QMessageBox::critical(
            nullptr, tr("Update"),
            tr("zsync cannot be found in path:\n") +
            "\"" + path + "\"\n" + tr("Unable to update."));
        emit isUpdated(false); // Sends the signal false
        return;
    }

    QString appimagepath = qgetenv("APPIMAGE"); // The path to AppImage
    QString appimagefollder(QFileInfo(appimagepath).absolutePath()); // The folder where AppImage is located
    qInfo() << "Path to AppImage: " << appimagepath;
    /*     */
    QProcess *p  = new QProcess(nullptr);  // A QProcess is created
    p->setWorkingDirectory(appimagefollder); // The folder where AppImage is located
    p->setProcessChannelMode(QProcess::MergedChannels);  /* QProcess merges the output of the running process into the standard output channel (stdout).
    The standard error channel (stderr) will not receive any data. The standard output and standard
    error data of the running process are interleaved. */
    p->start(path, arg); // zsync starts with arguments sent from calling class
    /* This signal is emitted when the process finishes. exitCode is the exit code of the process
     * (only valid for normal exits), and exitStatus is the exit status. After the process has finished,
     * the buffers in QProcess are still intact. You can still read any data that the process may
     * have written before it finished. */
    QString *result = new QString;
    result->clear();
    connect(p, &QProcess::readyReadStandardOutput, [ p, result]() {
        // Reads messages from zsync. And shows them in QMessageBox if something goes wrong.
        *result += p->readAllStandardOutput();
        qInfo() << "Output from zsync (1): " << *result;
    });

    connect(p, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this,
    [ = ](int exitCode) {
        if(exitCode != 0) {
            delete ud; // QDialog "Updating, please wait ..." is deletet
            QMessageBox::critical(
                nullptr, tr("Update"),
                tr("An unexpected error occurred. Error message:") + "\n" + *result);
            emit isUpdated(false); // Sends the signal false
            qInfo() << "Output from zsync (2), an error occured: " << *result;
            return;
        } else {
            // Everything has gone well, appropriate file permissions on the new AppImage are set
            delete ud; // QDialog "Updating, please wait ..." is deletet
            QMessageBox *msgBox = new QMessageBox(nullptr);
            QPushButton  *okButton = new QPushButton(tr("Ok"), msgBox);
            // okButton->setFixedSize(QSize(150, 40));
            msgBox->addButton(okButton, QMessageBox::YesRole);
            msgBox->setDefaultButton(okButton);
            msgBox->setWindowTitle(progname);
            msgBox->setText(progname + " " + tr("is updated."));
            msgBox->setWindowIcon(QIcon(*pixmap));
            msgBox->exec();
            qInfo() << "Output from zsync (3), everything went well: " << *result;

            if(msgBox->clickedButton() == okButton) {
                QString ny = appimagepath;
                QFile file1(ny);
                file1.setPermissions(QFileDevice::ReadUser | QFileDevice::ExeUser |
                                     QFileDevice::WriteUser | QFileDevice::ReadOther |
                                     QFileDevice::ExeOther);
                QString old = appimagepath + ".zs-old"; // The old AppImagen gets a new name
                QFile fileOld(old);
                qInfo() << "The old AppImagen is deleted: (4) " << fileOld.remove(); // The old AppImagen is deleted
                QStringList ARG;
                ARG << "--gui";
                qInfo() << "QProcess::startDetached(appimagepath, ARG) (5): " << QProcess::startDetached(appimagepath, ARG); // The new AppImagen is started
                emit isUpdated(true); // Sends the signal true
                return;
            }
        }
    });
}

bool Update::fileExists(QString & path)
{
    QFileInfo check_file(path);

    // check if file exists and if yes: Is it really a file and no directory?
    // and is it executable?
    if(check_file.exists() && check_file.isFile() && check_file.isExecutable()) {
        return true;
    } else {
        return false;
    }
}
