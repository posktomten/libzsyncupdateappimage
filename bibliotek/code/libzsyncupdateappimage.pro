#//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          zsyncUpdateAppImage
#//          Copyright (C) 2020 - 2023  Ingemar Ceicer
#//          https://gitlab.com/posktomten/zsyncupdateappimage
#//          ic_0002 (at) ceicer (dot) com
#//
#//   This program is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License version 3
#//   as published by the Free Software Foundation.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


HEADERS += update.h updatedialog.h
SOURCES += update.cpp updatedialog.cpp
FORMS += updatedialog.ui

TRANSLATIONS += i18n/_libupdateappimage_template_xx_XX.ts \
                i18n/_libupdateappimage_sv_SE.ts \
                i18n/_libupdateappimage_it_IT.ts

RESOURCES += \
    resource_libzsynkupdateappimage.qrc

TEMPLATE = lib

# By default, qmake will make a shared library.  Uncomment to make the library 
# static.
CONFIG += staticlib

# By default, TARGET is the same as the directory, so it will make 
# liblibrary.so or liblibrary.a (in linux).  Uncomment to override.




CONFIG(release, debug|release):BUILD=RELEASE
CONFIG(debug, debug|release):BUILD=DEBUG
CONFIG(dynamic, dynamic|static):message(dynamic)

equals(BUILD,RELEASE) {

    TARGET=updateappimage

}
equals(BUILD,DEBUG) {

    TARGET=updateappimaged
}
equals(QT_MAJOR_VERSION, 5) {
DESTDIR=$$OUT_PWD-lib
}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR=$$OUT_PWD-lib
}


UI_DIR = ../code



