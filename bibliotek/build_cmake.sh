#!/bin/bash
QT6=6.8.2
QT5=5.15.16
CURRENT_PATH=$(pwd)
DIR=build
EXECUTABLE=libappimageupdate
PATH=/opt/Qt/Tools/Ninja:${PATH}



PATH=/opt/Qt/${QT6}/gcc_64/bin:${PATH}
LD_LIBRARY_PATH=/opt/Qt/${QT6}/gcc_64/bin:${LD_LIBRARY_PATH}

echo Your current path: ${CURRENT_PATH}
echo Build directory: ${DIR}




cmake -S ${CURRENT_PATH}/code -B ${CURRENT_PATH}/${DIR} -G "Ninja" -DCMAKE_BUILD_TYPE=Release

cmake --build ${CURRENT_PATH}/${DIR} --target all

rm -r ${DIR}
