//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Test Program
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/zsyncupdateappimage
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "mainwindow.h"
// #include "ui_mainwindow.h"
#include "createshortcut.h"

void MainWindow::deleteallsettings()
{
    QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
    QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));
    QString *version = new QString(QStringLiteral(VERSION));

    if(Createshortcut::deleteAllSettings(display_name, executable_name, version)) {
        savesettings = false;
        close();
    }
}

void MainWindow::chortcutdesktop(int state)
{
    QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));

    if(state == 2) {
        QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
        QString *comments = new QString(QStringLiteral(COMMENTS));
        QString *categories = new QString(QStringLiteral(CATEGORIES));
        QString *icon  = new QString(QStringLiteral(EXECUTABLE_NAME".png"));
        Createshortcut::makeShortcutFile(display_name, executable_name, comments, categories, icon, false, true);
    } else {
        Createshortcut::removeDesktopShortcut(executable_name);
    }
}

void MainWindow::chortcutapplications(int state)
{
    QString *executable_name = new QString(QStringLiteral(EXECUTABLE_NAME));

    if(state == 2) {
        QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
        QString *comments = new QString(QStringLiteral(COMMENTS));
        QString *categories = new QString(QStringLiteral(CATEGORIES));
        QString *icon  = new QString(QStringLiteral(EXECUTABLE_NAME".png"));
        Createshortcut::makeShortcutFile(display_name, executable_name, comments, categories, icon, true, false);
    } else {
        Createshortcut::removeApplicationShortcut(executable_name);
    }
}
