//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Test Program
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/zsyncupdateappimage
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::setEndConfig()
{
    if(savesettings) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup(QStringLiteral("Settings"));
        settings.setValue(QStringLiteral("geometry"), saveGeometry());
        settings.setValue(QStringLiteral("state"), saveState());
        settings.endGroup();
        settings.beginGroup("Updates");
        settings.setValue("updateatstart", ui->checkCheckOnStart->isChecked());
        settings.endGroup();
        settings.beginGroup("Tools");
        settings.setValue("shortcutapplications", ui->checkShortcutApplications->isChecked());
        settings.setValue("shortcutdesktop", ui->checkShortcutDesktop->isChecked());
        settings.endGroup();
    }
}
