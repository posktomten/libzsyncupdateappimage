/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.14
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
// #include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionAbout;
    QAction *actionExit;
    QWidget *centralwidget;
    QFormLayout *formLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *pbCheckUpdate;
    QPushButton *pbUpdate;
    QPushButton *pbUpdateForce;
    QPushButton *pbDeleteSettings;
    QCheckBox *checkCheckOnStart;
    QCheckBox *checkShortcutApplications;
    QCheckBox *checkShortcutDesktop;
    QLabel *lblCompile;
    QMenuBar *menubar;
    QMenu *menuHelp;
    QMenu *menuFile;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(870, 227);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(13);
        MainWindow->setFont(font);
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        formLayout = new QFormLayout(centralwidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pbCheckUpdate = new QPushButton(centralwidget);
        pbCheckUpdate->setObjectName(QString::fromUtf8("pbCheckUpdate"));
        pbCheckUpdate->setMinimumSize(QSize(200, 0));
        pbCheckUpdate->setMaximumSize(QSize(600, 16777215));

        horizontalLayout->addWidget(pbCheckUpdate);

        pbUpdate = new QPushButton(centralwidget);
        pbUpdate->setObjectName(QString::fromUtf8("pbUpdate"));
        pbUpdate->setMinimumSize(QSize(200, 0));
        pbUpdate->setMaximumSize(QSize(600, 16777215));

        horizontalLayout->addWidget(pbUpdate);

        pbUpdateForce = new QPushButton(centralwidget);
        pbUpdateForce->setObjectName(QString::fromUtf8("pbUpdateForce"));
        pbUpdateForce->setMinimumSize(QSize(200, 0));
        pbUpdateForce->setMaximumSize(QSize(600, 16777215));

        horizontalLayout->addWidget(pbUpdateForce);

        pbDeleteSettings = new QPushButton(centralwidget);
        pbDeleteSettings->setObjectName(QString::fromUtf8("pbDeleteSettings"));
        pbDeleteSettings->setMinimumSize(QSize(200, 0));
        pbDeleteSettings->setMaximumSize(QSize(600, 16777215));

        horizontalLayout->addWidget(pbDeleteSettings);


        formLayout->setLayout(0, QFormLayout::LabelRole, horizontalLayout);

        checkCheckOnStart = new QCheckBox(centralwidget);
        checkCheckOnStart->setObjectName(QString::fromUtf8("checkCheckOnStart"));
        checkCheckOnStart->setChecked(true);

        formLayout->setWidget(1, QFormLayout::LabelRole, checkCheckOnStart);

        checkShortcutApplications = new QCheckBox(centralwidget);
        checkShortcutApplications->setObjectName(QString::fromUtf8("checkShortcutApplications"));

        formLayout->setWidget(2, QFormLayout::LabelRole, checkShortcutApplications);

        checkShortcutDesktop = new QCheckBox(centralwidget);
        checkShortcutDesktop->setObjectName(QString::fromUtf8("checkShortcutDesktop"));

        formLayout->setWidget(3, QFormLayout::LabelRole, checkShortcutDesktop);

        lblCompile = new QLabel(centralwidget);
        lblCompile->setObjectName(QString::fromUtf8("lblCompile"));

        formLayout->setWidget(4, QFormLayout::LabelRole, lblCompile);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 870, 25));
        menuHelp = new QMenu(menubar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuHelp->menuAction());
        menuHelp->addAction(actionAbout);
        menuFile->addAction(actionExit);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        actionAbout->setText(QCoreApplication::translate("MainWindow", "About...", nullptr));
        actionExit->setText(QCoreApplication::translate("MainWindow", "Exit", nullptr));
#if QT_CONFIG(shortcut)
        actionExit->setShortcut(QCoreApplication::translate("MainWindow", "F4", nullptr));
#endif // QT_CONFIG(shortcut)
        pbCheckUpdate->setText(QCoreApplication::translate("MainWindow", "Check for updates", nullptr));
        pbUpdate->setText(QCoreApplication::translate("MainWindow", "Update", nullptr));
        pbUpdateForce->setText(QCoreApplication::translate("MainWindow", "Force Update", nullptr));
        pbDeleteSettings->setText(QCoreApplication::translate("MainWindow", "Delete Settings", nullptr));
        checkCheckOnStart->setText(QCoreApplication::translate("MainWindow", "Check for updates at program start", nullptr));
        checkShortcutApplications->setText(QCoreApplication::translate("MainWindow", "Shortcut in the operatin systems menu system", nullptr));
        checkShortcutDesktop->setText(QCoreApplication::translate("MainWindow", "Desktop shortcut", nullptr));
        lblCompile->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        menuHelp->setTitle(QCoreApplication::translate("MainWindow", "Help", nullptr));
        menuFile->setTitle(QCoreApplication::translate("MainWindow", "File", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
