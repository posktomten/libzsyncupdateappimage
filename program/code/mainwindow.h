//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          zsyncUpdateAppImage
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/zsyncupdateappimage
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef MAINWINDOW_H
#define MAINWINDOW_H



#include <QMainWindow>
#include <QtGlobal> // Q_PROCESSOR_WORDSIZE
#include <QSettings>
#include <QStandardPaths>
#include <QDir>
#include <QFontDatabase>
#include <QFileInfo>
#include <QMessageBox>
#include "updatedialog.h"
#include "checkupdate.h"

#define VERSION "0.0.12"
#define DISPLAY_NAME "Test Program"
#define EXECUTABLE_NAME "testprg"
#define VERSION_PATH "http://bin.ceicer.com/testprg/version.txt"

#define COPYRIGHT "Ingemar Ceicer"
#define COPYRIGHT_YEAR "2020"
#define EMAIL "programming@ceicer.com"
#define BUILD_DATE_TIME __DATE__ " " __TIME__
#define LICENSE "Gnu General Public License Version 3"
#define LICENSE_LINK "https://gitlab.com/posktomten/libzsyncupdateappimage/-/blob/master/LICENSE"
#define CHANGELOG "https://gitlab.com/posktomten/libzsyncupdateappimage/-/blob/master/CHANGELOG"
#define SOURCECODE "https://gitlab.com/posktomten/libzsyncupdateappimage"
#define WEBSITE "https://gitlab.com/posktomten/libzsyncupdateappimage/-/wikis/home"
/***/
// chortcut
#define COMMENTS "Program to test AppImage that updates itself."
#define CATEGORIES "Utility"

/***/
#define FONTSIZE 12

#if defined(Q_PROCESSOR_X86_64) // 64 bit

#if (__GLIBC_MINOR__ == 27)
#define COMPILEDON "Lubuntu 18.04.6 LTS 64-bit, GLIBC 2.27"
#define ARG1 "testprg-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/testprg/GLIBC2.27/testprg-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 27

#if (__GLIBC_MINOR__ == 31)
#define COMPILEDON "Lubuntu 20.04.6 LTS 64-bit, GLIBC 2.31"
#define ARG1 "testprg-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/testprg/GLIBC2.31/testprg-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 31

#if  (__GLIBC_MINOR__ == 35)
#define COMPILEDON "Ubuntu 22.04.4 LTS 64-bit, GLIBC 2.35"
#define ARG1 "testprg-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/testprg/GLIBC2.35/testprg-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 35

#if  (__GLIBC_MINOR__ == 39)
#define COMPILEDON "Lubuntu 24.04 LTS 64-bit, GLIBC 2.39"
#define ARG1 "testprg-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/testprg/GLIBC2.39/testprg-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 39

#elif defined(Q_PROCESSOR_X86_32) // 32 bit

#define COMPILEDON "Lubuntu 18.04.6 LTS 32-bit, GLIBC 2.27"
#if defined(FFMPEG)
#define ARG1 "testprg-ffmpeg-i386.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/testprg/GLIBC2.27/testprg-ffmpeg-i386.AppImage.zsync"
#elif !defined(FFMPEG)
#define ARG1 "testprg-i386.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/testprg/GLIBC2.27/testprg-i386.AppImage.zsync"
#endif

#endif // Q_PROCESSOR_



#define BUILD_DATE_TIME __DATE__ " " __TIME__


QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}

QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setEndConfig();



private:
    Ui::MainWindow *ui;
    UpdateDialog *ud;
    void doCheckForUpdates();
    void checkOnStart();
    QString UpdateInstructions();
    void updateNeeded(bool needed);
    void chortcutdesktop(int state);
    void chortcutapplications(int state);
    void deleteallsettings();



    void firstRun();
    void about();
    bool savesettings;



public slots:
//    Receives a Boolean value from the "Update" class.
//    True if the update was successful and false if the update failed
    void isUpdated(bool uppdated);
};

#endif // MAINWINDOW_H
