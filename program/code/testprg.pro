#//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          Testprogram
#//          Copyright (C) 2021 - 2023 Ingemar Ceicer
#//          https://gitlab.com/posktomten/zsyncupdateappimage
#//          ic_0002 (at) ceicer (dot) com
#//
#//   This program is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License version 3
#//   as published by the Free Software Foundation.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#CONFIG += c++11
TARGET = testprg


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    chortcuts.cpp \
    firstrun.cpp \
    lastrun.cpp \
    main.cpp \
    mainwindow.cpp \
    updates.cpp \
    about.cpp

HEADERS += \
    mainwindow.h 


FORMS += \
    mainwindow.ui 

TRANSLATIONS += i18n/testprg_xx_XX.ts \
                i18n/testprg_sv_SE.ts
    
    
    
    

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


RESOURCES += \
    resource.qrc

CODECFORSRC = UTF-8
UI_DIR = ../code
INCLUDEPATH += "../include"
DEPENDPATH += "../include"

    equals(QT_MAJOR_VERSION, 5) {
    DESTDIR="../build-executable5"

    CONFIG (release, debug|release): LIBS += -L../lib5/ -lcheckupdate  # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcheckupdated # Debug
    CONFIG (release, debug|release): LIBS += -L../lib5/ -lupdateappimage # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lupdateappimaged # Debug
    CONFIG (release, debug|release): LIBS += -L../lib5/ -labout # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -laboutd # Debug
    CONFIG (release, debug|release): LIBS += -L../lib5/ -lcreateshortcut # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcreateshortcutd # Debug

    }

    equals(QT_MAJOR_VERSION, 6) {
    DESTDIR="../build-executable6"

    CONFIG (release, debug|release): LIBS += -L../lib6/ -lcheckupdate  # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -lcheckupdated # Debug
    CONFIG (release, debug|release): LIBS += -L../lib6/ -lupdateappimage # Release
    else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -lupdateappimaged # Debug

    }



