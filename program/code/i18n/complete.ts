<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="../checkforupdates.cpp" line="49"/>
        <location filename="../checkforupdates.cpp" line="121"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittades.
Kontrollera dina Internetinställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="66"/>
        <location filename="../checkforupdates.cpp" line="74"/>
        <source>Your version of </source>
        <translation>Din version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="67"/>
        <source> is newer than the latest official version. </source>
        <translation> är nyare än den senaste officiella versionen. </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="75"/>
        <source> is the same version as the latest official version. </source>
        <translation> är samma version som den senaste officiella versionen. </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="84"/>
        <source>There was an error when the version was checked.</source>
        <translation>Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="136"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="177"/>
        <source>Updates:</source>
        <translation>Uppdateringar:</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="198"/>
        <source>There is a new version of </source>
        <translation>Det finns en ny version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="200"/>
        <source>Latest version: </source>
        <translation>Senaste versionen: </translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="14"/>
        <source>Test Program</source>
        <translation>Testprogram</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="20"/>
        <source>Check for updates</source>
        <translation>Sök efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="27"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="27"/>
        <source>Compiled </source>
        <translation>Kompilerad </translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="30"/>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Uppdatera&quot; för att uppdatera.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="35"/>
        <source>Download a new</source>
        <translation>Ladda ner en ny</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="38"/>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update component&quot;.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Underhållsverktyg&quot; och &quot;Update component&quot;.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="84"/>
        <source>true sent from library</source>
        <translation>true skickat från bibliotek</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="87"/>
        <source>false sent from library</source>
        <translation>false skickat från bibliotek</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="94"/>
        <source>At start: true sent from library</source>
        <translation>Vid start: true skickat från bibliotek</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="97"/>
        <source>At start: false sent from library</source>
        <translation type="unfinished">Vid start: false skickat från bibliotek</translation>
    </message>
    <message>
        <source>Found updates</source>
        <translation type="vanished">Hittade uppdateringar</translation>
    </message>
    <message>
        <source>Found updates at program startup</source>
        <translation type="vanished">Hittade uppdateringar när programmet startade</translation>
    </message>
    <message>
        <source>Found update</source>
        <translation type="vanished">Hittade uppdateringar</translation>
    </message>
    <message>
        <source>found update at program startup</source>
        <translation type="vanished">hittade uppdateringar när programmet startade</translation>
    </message>
</context>
</TS>
