//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Test Program
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/zsyncupdateappimage
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "mainwindow.h"
#include "ui_mainwindow.h"


void MainWindow::firstRun()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
//    settings.setIniCodec("UTF-8");
    settings.beginGroup("FirstRun");
    bool firstrun = settings.value("firstrun", true).toBool();
    settings.endGroup();
    settings.beginGroup("FirstRun");
    settings.setValue("firstrun", firstrun);
    settings.endGroup();
    settings.sync();

    if(!firstrun) {
        return;
    }

#ifdef Q_OS_LINUX
    QString inifile = settings.fileName();
    QFileInfo fi(inifile);
    QString iconpath = fi.absolutePath();
    QDir pathDir(iconpath);

    if(pathDir.exists()) {
        iconpath.append("/testprg.png");
        const QString pngpath = QCoreApplication::applicationDirPath() + "/testprg.png";

        if(QFile::copy(pngpath, iconpath)) {
            QFile file(iconpath);

            if(file.exists())
                file.setPermissions(QFileDevice::ReadUser | QFileDevice::WriteUser | QFileDevice::ReadOther);
        } else {
            QMessageBox::information(nullptr, DISPLAY_NAME " " VERSION,
                                     tr("Unable to copy icon file"));
        }
    }

#endif
    chortcutapplications(2);
    settings.beginGroup("FirstRun");
    settings.setValue("firstrun", false);
    settings.endGroup();
    /*    settings.sync();
        QMessageBox::information(nullptr, DISPLAY_NAME " " VERSION,
                                 tr("Welcome to Test Program!"))*/;
}
