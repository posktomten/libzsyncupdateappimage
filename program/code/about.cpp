//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Program name
//          Copyright (C) 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "mainwindow.h"
#include "about.h"
void MainWindow::about()
{
    const QString *purpose = new QString(QStringLiteral(""));
    QString *copyright_year = new QString;

    if(COPYRIGHT_YEAR == QString::number(QDate::currentDate().year())) {
        *copyright_year = QString::number(QDate::currentDate().year());
    } else {
        *copyright_year = COPYRIGHT_YEAR " - " + QString::number(QDate::currentDate().year());
    }

    // QString::number(QDate::currentDate().year());
    const QString *translator = new QString(QStringLiteral(""));
    const QPixmap *pixmap = new QPixmap(QStringLiteral(":/images/icon.png"));
    // const QPixmap *pixmap = nullptr;
    About *about = new About(QStringLiteral(DISPLAY_NAME),
                             QStringLiteral(VERSION),
                             QStringLiteral(COPYRIGHT),
                             QStringLiteral(EMAIL),
                             copyright_year,
                             QStringLiteral(BUILD_DATE_TIME),
                             QStringLiteral(LICENSE),
                             QStringLiteral(LICENSE_LINK),
                             QStringLiteral(CHANGELOG),
                             QStringLiteral(SOURCECODE),
                             QStringLiteral(WEBSITE),
                             QStringLiteral(COMPILEDON),
                             purpose,
                             translator,
                             pixmap,
                             false
                            );
    delete about;
}
