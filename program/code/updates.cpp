//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Test Program
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/zsyncupdateappimage
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "mainwindow.h"
#include "ui_mainwindow.h"
void MainWindow::doCheckForUpdates()
{
    const QString updateinstructions = UpdateInstructions();
    QIcon *icon = new QIcon(":/image/testprg.png");
    CheckUpdate *cu = new CheckUpdate ;
    cu->move(this->x(), this->y());
    cu->check(DISPLAY_NAME, VERSION, VERSION_PATH, updateinstructions, icon);
    connect(cu, &CheckUpdate ::foundUpdate, [this](bool b) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);

        if(b) {
            // QMessageBox::information(this, DISPLAY_NAME " " VERSION, tr("Updates found"));
            updateNeeded(true);
        } else {
            updateNeeded(false);
//            QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("No need for updates"));
        }
    });
}

void MainWindow::checkOnStart()
{
    const QString updateinstructions = UpdateInstructions();
    QIcon *icon = new QIcon(":/image/testprg.png");
    CheckUpdate  *cu = new CheckUpdate;
    cu->checkOnStart(DISPLAY_NAME, VERSION, VERSION_PATH, updateinstructions, icon);
    connect(cu, &CheckUpdate ::foundUpdate, [this](bool b) {
//        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
//                           EXECUTABLE_NAME);
//        settings.setIniCodec("UTF-8");
        if(b) {
            updateNeeded(true);
#ifdef Q_OS_LINUX
//            QMessageBox::information(this, DISPLAY_NAME " " VERSION, tr("Updates found"));
#endif
        } else {
            updateNeeded(false);
#ifdef Q_OS_LINUX
//            QMessageBox::critical(this, DISPLAY_NAME " " VERSION, tr("No need for updates"));
#endif
        }
    });
}

QString MainWindow::UpdateInstructions()
{
#ifdef Q_OS_LINUX
    /*    QString *updateinstructions =
            new QString(tr("Select \"Tools\", \"Update\" to update."))*/;
    QString updateinstructions(tr("Select \"Tools\", \"Update\" to update."));
#endif
#ifdef Q_OS_WIN
#ifdef portable
    QString *updateinstructions =
        new QString(QObject::tr("Download a new") + " <a href=\"" DOWNLOAD_PATH
                    "\"> portable</a>");
#endif
#ifndef portable
    QString *updateinstructions = new QString(QObject::tr(
            "Select \"Tools\", \"Update / Uninstall\" and \"Update component\"."));
#endif
#endif
    return updateinstructions;
}

void MainWindow::updateNeeded(bool needed)
{
    if(needed) {
        ui->pbUpdate->setEnabled(true);
    } else {
        ui->pbUpdate->setDisabled(true);
    }

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Updates");
    settings.setValue("updateneeded", needed);
    settings.endGroup();
}
