//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Test Program
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/zsyncupdateappimage
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "mainwindow.h"

#include <QApplication>
// For translation
//#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
//    For translation
//    "complete.qm" contains translations from libzsyncupdateappimage and from your application.
//    QTranslator translator;
//    const QString translationPath = ":/i18n/complete.qm";
//    if(translator.load(translationPath)) {
//        QApplication::installTranslator(&translator);
//    }
//
    QString fontPathRegular = ":/fonts/PTSans-Regular.ttf";
    int fontidRegular = QFontDatabase::addApplicationFont(fontPathRegular);
    QString regular = QFontDatabase::applicationFontFamilies(fontidRegular).at(0);
    QFont fregular(regular, FONTSIZE);
    QApplication::setFont(fregular);
    MainWindow *w = new MainWindow;
    QObject::connect(&a, &QApplication::aboutToQuit, w,  &MainWindow::setEndConfig);
    w->show();
    QObject::connect(&a, &QCoreApplication::aboutToQuit,
                     [w]() -> void { w->setEndConfig(); });
    return a.exec();
}
