//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Test Program
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/zsyncupdateappimage
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileIconProvider>
#include "update.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    // Q_INIT_RESOURCE(resource_libzsynkupdateappimage);
    ui->setupUi(this);
    savesettings = true;
    firstRun();
    QString fontPathRegular = ":/fonts/PTSans-Regular.ttf";
    int fontidRegular = QFontDatabase::addApplicationFont(fontPathRegular);
    QString regular = QFontDatabase::applicationFontFamilies(fontidRegular).at(0);
    QFont fregular(regular, FONTSIZE);
    this->setFont(fregular);
    this->setWindowTitle(tr("Test Program") + " "  VERSION);
    this->setWindowIcon(QIcon(":/image/testprg.png"));
    this->move(200, 200);
    ui->lblCompile->setText(tr("Compiled ") + BUILD_DATE_TIME);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
//    settings.setIniCodec("UTF-8");
    settings.beginGroup("Updates");
    bool updateatstart = settings.value("updateatstart", true).toBool();
    settings.endGroup();
    settings.beginGroup("Tools");
    ui->checkCheckOnStart->setChecked(updateatstart);
    ui->checkShortcutApplications->setChecked(settings.value("shortcutapplications", false).toBool());
    ui->checkShortcutDesktop->setChecked(settings.value("shortcutdesktop", false).toBool());
    settings.endGroup();
    settings.beginGroup(QStringLiteral("Settings"));
    this->restoreState(settings.value(QStringLiteral("state")).toByteArray());
    this->restoreGeometry(settings.value(QStringLiteral("geometry")).toByteArray());
    settings.endGroup();

    /* Check for updates then the program start */
    if(updateatstart) {
        checkOnStart();
    }

    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::close);
    /* Code for Check for updates */
    connect(ui->pbCheckUpdate, &QPushButton::clicked, this, &MainWindow::doCheckForUpdates);
    /* End Code for Check for updates */
    /* Code for Check for About */
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::about);
    /* End Code for Check for About */
    /*   */
    /*   */
    /* Code for update */
    connect(ui->pbUpdate, &QPushButton::clicked, [this]() ->void {

        QIcon *icon = new QIcon(":/image/testprg.png");

        Update *up = new Update;
        connect(up, &Update::isUpdated, this, &MainWindow::isUpdated);

        // QDialog class
        ud = new UpdateDialog;


        ud->viewUpdate(icon);
        ud->show();
        ud->move(this->x(), this->y());

        up->doUpdate(ARG1, ARG2, DISPLAY_NAME, ud, icon);


    });
    connect(ui->pbUpdateForce, &QPushButton::clicked, [this]() ->void {
        QMetaObject::invokeMethod(ui->pbUpdate, "clicked");

    });
    /* End Code for update */
    /* Code for icons and shortcuts */
    // Shortcut for menu
    connect(ui->checkShortcutApplications, &QCheckBox::stateChanged, this, &MainWindow::chortcutapplications);
    // Shortcut for Desktop
    connect(ui->checkShortcutDesktop, &QCheckBox::stateChanged, this, &MainWindow::chortcutdesktop);
    /*
     *
     * Delete all settings */
    // Do you want to delete all saved settings and shortcuts?\nThey cannot be restored.
    connect(ui->pbDeleteSettings, &QPushButton::clicked, this,  &MainWindow::deleteallsettings);
}

/* slot to receive the signal from the class "Update" */
void MainWindow::isUpdated(bool isupdated)
{
//    If the update is successful, the old program will close.
    if(isupdated) {
        close();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
