#!/bin/bash


# EDIT
QT5=5.15.8
QT6=6.4.3
EXECUTABLE=zsyncupdateappimage
SUPORTEDGLIBC=2.27
READMORE=https://gitlab.com/posktomten/${EXECUTABLE}
# STOP EDIT

echo "----------------------------------------------------------------"
GLIBCVERSION=`glibcversion`
echo "GLIBC version=${GLIBCVERSION}"
echo "suported GLIBC=${SUPORTEDGLIBC}"


echo "----------------------------------------------------------------"
PS3='Please enter your choice: '
options=("Qt5.15.2" "Qt${QT5}" "Qt${QT5}z2" "Qt${QT5} unsuported" "Qt${QT6}" "Qt${QT6}z2" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Qt5.15.2 64-bit")
            export LD_LIBRARY_PATH="/opt/Qt/5.15.2/gcc_64/lib:${LD_LIBRARY_PATH}"
            export PATH="/opt/Qt/5.15.2/gcc_64/bin:${PATH}"
            APPDIR="AppDirQt5"
            echo "You chose $REPLY which is $opt"
            qmakeexecutable="/opt/Qt/5.15.2/gcc_64/bin/qmake"
            BIN_DIR="/bin.ceicer.com/public_html/${EXECUTABLE}/bin/linux/GLIBC${GLIBCVERSION}"
            ZSYNC_ADDRESS="http://bin.ceicer.com/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}/"
            ZSYNC_DIR="/bin.ceicer.com/public_html/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}"
            break;
            ;;
        "Qt${QT5}")
            export LD_LIBRARY_PATH="/opt/Qt/Qt${QT5}/lib:${LD_LIBRARY_PATH}"
            export PATH="/opt/Qt/Qt${QT5}/bin:${PATH}"
            APPDIR="AppDirQt5"
            echo "You chose $REPLY which is $opt"
            qmakeexecutable="/opt/Qt/Qt${QT5}/bin/qmake"
            unsuported="-executable=${APPDIR}/usr/bin/zsync"
            BIN_DIR="/bin.ceicer.com/public_html/${EXECUTABLE}/bin/linux/GLIBC${GLIBCVERSION}"
            ZSYNC_ADDRESS="http://bin.ceicer.com/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}/"
            ZSYNC_DIR="/bin.ceicer.com/public_html/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}"
            break;
            ;;
        "Qt${QT5}z2")
            export LD_LIBRARY_PATH="/opt/Qt/Qt${QT5}/lib:${LD_LIBRARY_PATH}"
            export PATH="/opt/Qt/Qt${QT5}/bin:${PATH}"
            APPDIR="AppDirQt5z2"
            echo "You chose $REPLY which is $opt"
            qmakeexecutable="/opt/Qt/Qt${QT5}/bin/qmake"
            unsuported="-executable=${APPDIR}/usr/bin/zsync"
            BIN_DIR="/bin.ceicer.com/public_html/${EXECUTABLE}/bin/linux/zsync2/GLIBC${GLIBCVERSION}"
            ZSYNC_ADDRESS="https://bin.ceicer.com/zsync2/${EXECUTABLE}/GLIBC${GLIBCVERSION}/"
            ZSYNC_DIR="/bin.ceicer.com/public_html/zsync2/${EXECUTABLE}/GLIBC${GLIBCVERSION}"
            break;
            ;;
        "Qt${QT5} unsuported")
            export LD_LIBRARY_PATH="/opt/Qt/Qt${QT5}/lib:${LD_LIBRARY_PATH}"
            export PATH="/opt/Qt/Qt${QT5}/bin:${PATH}"
            APPDIR="AppDirQt5"
            echo "You chose $REPLY which is $opt"
            qmakeexecutable="/opt/Qt/Qt${QT5}/bin/qmake"
            unsuported="-executable=${APPDIR}/usr/bin/zsync -unsupported-allow-new-glibc"
            BIN_DIR="/bin.ceicer.com/public_html/${EXECUTABLE}/bin/linux/GLIBC${GLIBCVERSION}"
            ZSYNC_ADDRESS="http://bin.ceicer.com/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}/"
            ZSYNC_DIR="/bin.ceicer.com/public_html/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}"
            break;
            ;;
        "Qt${QT6}")
            export LD_LIBRARY_PATH="/opt/Qt/${QT6}/gcc_64/lib:${LD_LIBRARY_PATH}"
            export PATH="/opt/Qt/${QT6}/gcc_64/bin:${PATH}"
            APPDIR="AppDirQt6"
            echo "You chose $REPLY which is $opt"
            qmakeexecutable="/opt/Qt/${QT6}/gcc_64/bin/qmake"
            unsuported="-executable=${APPDIR}/usr/bin/zsync -extra-plugins=${APPDIR}/usr/plugins/platforms/libqxcb.so -unsupported-allow-new-glibc"
            BIN_DIR="/bin.ceicer.com/public_html/${EXECUTABLE}/bin/linux/GLIBC${GLIBCVERSION}"
            ZSYNC_ADDRESS="http://bin.ceicer.com/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}/"
            ZSYNC_DIR="/bin.ceicer.com/public_html/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}"
            break;
            ;;
        "Qt${QT6}z2")
            export LD_LIBRARY_PATH="/opt/Qt/${QT6}/gcc_64/lib:${LD_LIBRARY_PATH}"
            export PATH="/opt/Qt/${QT6}/gcc_64/bin:${PATH}"
            APPDIR="AppDirQt6z2"
            echo "You chose $REPLY which is $opt"
            qmakeexecutable="/opt/Qt/${QT6}/gcc_64/bin/qmake"
            unsuported="-executable=${APPDIR}/usr/bin/zsync -extra-plugins=${APPDIR}/usr/plugins/platforms/libqxcb.so -unsupported-allow-new-glibc"
            BIN_DIR="/bin.ceicer.com/public_html/${EXECUTABLE}/bin/linux/zsync2/GLIBC${GLIBCVERSION}"
            ZSYNC_ADDRESS="https://bin.ceicer.com/zsync2/${EXECUTABLE}/GLIBC${GLIBCVERSION}/"
            ZSYNC_DIR="/bin.ceicer.com/public_html/zsync2/${EXECUTABLE}/GLIBC${GLIBCVERSION}"
            break;
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Sign AppImage" "Do not sign AppImage" "Quit")
select opt in "${options[@]}"
do


	case $opt in
		"Sign AppImage")
			echo "You chose $REPLY which is $opt"
			signera=1
			break;
			;;
		"Do not sign AppImage")
			echo "You chose $REPLY which is $opt"
			signera=0  
			break;
			;;
		"Quit")
			break
			;;
		*) echo "invalid option $REPLY";;
	esac
done
echo "----------------------------------------------------------------"
PS3='Please enter your choice: '
options=("AppImage" "Quit")
select opt in "${options[@]}"
do

    case $opt in
        "AppImage")
            echo "You chose $REPLY which is $opt"
            ~/bin/linuxdeployqt ${APPDIR}/usr/bin/${EXECUTABLE} -qmake=$qmakeexecutable -bundle-non-qt-libs -verbose=1 -no-translations -no-copy-copyright-files -always-overwrite $unsuported
          #  ~/bin/linuxdeployqt ${APPDIR_FFMPEG}/usr/bin/${EXECUTABLE} -qmake=$qmakeexecutable -bundle-non-qt-libs -verbose=1 -no-translations -no-copy-copyright-files -always-overwrite $unsuported_ffmpeg

         
            
            if [ "$signera" -gt 0 ] ;
            then
				~/bin/appimagetool --sign ${APPDIR}
			else
				~/bin/appimagetool ${APPDIR}
			fi
			
        #    if [ "$signera" -gt 0 ] ;
        #    then
		#		~/bin/appimagetool --sign ${APPDIR_FFMPEG}
		#	else
		#		~/bin/appimagetool ${APPDIR_FFMPEG}
		#	fi
           
            break
            ;;
        "Quit")
        echo "Goodbye"
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done


date_now=$(date "+%F %H-%M-%S")

for i in *.AppImage; do # Whitespace-safe but not recursive.

   

    echo "-------------------------------------------------------------------------------" > "$i-MD5.txt"
	echo "MD5 HASH SUM" >> "$i-MD5.txt"
	md5sum "$i" >> "$i-MD5.txt"
    echo "-------------------------------------------------------------------------------" >> "$i-MD5.txt"
	filesize=$(stat -c%s "$i")
	mb=$(bc <<< "scale=6; $filesize / 1048576")
	mb2=$(echo $mb | awk '{printf "%f", $1 + $2}')
	echo "Uploaded: $date_now" >> "$i-MD5.txt"
	echo "Size: $mb MB." >> "$i-MD5.txt"
	echo "$i" requires GLIBC higher than or equal to "${GLIBCVERSION}" >> "$i-MD5.txt"
    echo "-------------------------------------------------------------------------------" >> "$i-MD5.txt"
	echo "HOW TO CHECK THE HASH SUM" >> "$i-MD5.txt"
    echo "-------------------------------------------------------------------------------" >> "$i-MD5.txt"
	echo "LINUX" >> "$i-MD5.txt"
	echo "\"md5sum\" is included in most Linus distributions." >> "$i-MD5.txt"
	echo "md5sum $i" >> "$i-MD5.txt"
    echo "-------------------------------------------------------------------------------" >> "$i-MD5.txt"
	echo "WINDOWS" >> "$i-MD5.txt"
	echo "\"certutil\" is included in Windows." >> "$i-MD5.txt"
	echo "certutil -hashfile $i md5" >> "$i-MD5.txt"
    echo "-------------------------------------------------------------------------------" >> "$i-MD5.txt"
	echo "READ MORE" >> "$i-MD5.txt"
	echo "$READMORE" >> "$i-MD5.txt"
    echo "-------------------------------------------------------------------------------" >> "$i-MD5.txt"
    
    
done


echo "----------------------------------------------------------------"
PS3='Please enter your choice: '
options=("Upload AppImage" "Upload AppImage later" "Quit")
select opt in "${options[@]}"
do


case $opt in
    "Upload AppImage")
        echo "You chose $REPLY which is $opt"
        
 cp -f *.AppImage ${EXECUTABLE}-zsyncmake/
 cp -f *.AppImage-MD5.txt ${EXECUTABLE}-zsyncmake/
 cd ${EXECUTABLE}-zsyncmake
 
if [[ $APPDIR == *"z2" ]]; then
  export LD_LIBRARY_PATH="lib"
fi


for i in *.AppImage; do # Whitespace-safe but not recursive.

if [[ $APPDIR == *"z2" ]]; then
  ./zsyncmake2 "$i" -u "${ZSYNC_ADDRESS}/${i}" 
else
   ./zsyncmake "$i" -u "${ZSYNC_ADDRESS}/${i}" 
fi

done



HOST="`sed -n 1p ../../../secret`"
USER="`sed -n 2p ../../../secret`"
PASSWORD="`sed -n 3p ../../../secret`"



for fil in *.AppImage; do # Whitespace-safe but not recursive.

ftp -v -inv $HOST <<EOF
passive
user $USER $PASSWORD
cd "$BIN_DIR"
put $fil
cd "$ZSYNC_DIR"
put $fil
bye
EOF

done



for fil in *.zsync; do # Whitespace-safe but not recursive.

ftp -v -inv $HOST <<EOF
passive
user $USER $PASSWORD
cd "$ZSYNC_DIR"
put $fil
bye
EOF

done

for fil in *AppImage-MD5.txt; do # Whitespace-safe but not recursive.

ftp -v -inv $HOST <<EOF
passive    
user $USER $PASSWORD
cd "$BIN_DIR"
put $fil
bye
EOF

done
 break
 ;;

"Upload AppImage later")
	 cp -f *.AppImage ${EXECUTABLE}-zsyncmake/
     cd ${EXECUTABLE}-zsyncmake
     
if [[ $APPDIR == *"z2" ]]; then
  export LD_LIBRARY_PATH="lib"
fi
     

for i in *.AppImage; do # Whitespace-safe but not recursive.

if [[ $APPDIR == *"z2" ]]; then
  ./zsyncmake2 "$i" -u "${ZSYNC_ADDRESS}/${i}" 
else
   ./zsyncmake "$i" -u "${ZSYNC_ADDRESS}/${i}" 
fi
done
	break
	;;

"Quit")
	echo "Goodbye"
	break
	;;
	*) echo "invalid option $REPLY"
	;;
	esac
done
