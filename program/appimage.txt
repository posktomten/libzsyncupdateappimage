linuxdeployqt
appimagetool

-unsupported-allow-new-glibc
-unsupported-bundle-everything



Lubuntu 64 bit dynamic

Qt6.4.2
export LD_LIBRARY_PATH=/opt/Qt/6.4.2/gcc_64/lib:$LD_LIBRARY_PATH
export PATH=/opt/Qt/6.4.2/gcc_64/bin:$PATH
linuxdeployqt AppDir/usr/bin/zsyncupdateappimage -qmake=/opt/Qt/6.4.2/gcc_64/bin/qmake -bundle-non-qt-libs -verbose=2 -no-translations -no-copy-copyright-files -unsupported-allow-new-glibc


Qt5.15.8
export LD_LIBRARY_PATH=/opt/Qt/Qt5.15.8/lib:$LD_LIBRARY_PATH
export PATH=/opt/Qt/Qt5.15.8/bin:$PATH
linuxdeployqt AppDir/usr/bin/zsyncupdateappimage -qmake=/opt/Qt/Qt5.15.8/bin/qmake -bundle-non-qt-libs -verbose=2 -no-translations -no-copy-copyright-files -unsupported-allow-new-glibc





appimagetool --sign AppDir/
appimagetool -n --sign AppDir/


Lubuntu 32, dynamic


Lubuntu 32, static


Path to qmake
Lubuntu 16.04 64 bit
/opt/Qt/5.15.2/gcc_64/bin/qmake

Lubuntu 16.04 32 bit Dynamic
/opt/Qt5.15.2_shared/bin/qmake

Lubuntu 16.04 32 bit Static
/opt/Qt5.15.2_static/bin/qmake



Lubuntu 32-bit DYNAMIC
/opt/Qt5.15.2_shared/bin/lrelease ../code/libzsyncupdateappimage.pro
/opt/Qt5.15.2_shared/bin/qmake -project ../code/libzsyncupdateappimage.pro
/opt/Qt5.15.2_shared/bin/qmake ../code/libzsyncupdateappimage.pro
make

Lubuntu 32-bit STATIC
/opt/Qt5.15.2_static/bin/lrelease ../code/libzsyncupdateappimage.pro
/opt/Qt5.15.2_static/bin/qmake -project ../code/libzsyncupdateappimage.pro
/opt/Qt5.15.2_static/bin/qmake ../code/libzsyncupdateappimage.pro
make



Lubuntu 32-bit DYNAMIC
/opt/Qt5.15.2_shared/bin/lrelease ../code/zsyncupdateappimage.pro
/opt/Qt5.15.2_shared/bin/qmake -project ../code/zsyncupdateappimage.pro
/opt/Qt5.15.2_shared/bin/qmake ../code/zsyncupdateappimage.pro
make

Lubuntu 32-bit STATIC
/opt/Qt5.15.2_static/bin/lrelease ../code/zsyncupdateappimage.pro
/opt/Qt5.15.2_static/bin/qmake -project ../code/zsyncupdateappimage.pro
/opt/Qt5.15.2_static/bin/qmake ../code/zsyncupdateappimage.pro
make


Ubuntu 20.10 glibc 2.32
Ubuntu 20.04 glibc 2.31
Ubuntu 18.04 GLIBC 2.27 2018-02-01
Ubuntu 17.10 glibc 2.26
Ubuntu 16.04 glibc 2.23
Ubuntu 14.04 glibc 2.19

