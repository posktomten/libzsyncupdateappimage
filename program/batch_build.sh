#!/bin/bash

PROGNAME=testprg
QT5=5.15.14

		echo -----------------------------------------------------------
		PS3='Please enter your choice: '
		options=("Qt5.15.2 64-bit" "Qt${QT5} 64-bit" "Qt${QT5} 32-bit" "Quit")
		select opt in "${options[@]}"
do
    case $opt in
        "Qt5.15.2 64-bit")
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/5.15.2/gcc_64/bin/qmake"
            build_executable="build-executable5"
            break;
            ;;
        "Qt${QT5} 64-bit")
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/${QT5}/gcc_64/bin/qmake"
            build_executable="build-executable5"
            export LD_LIBRARY_PATH=/opt/Qt/${QT5}/gcc_64/lib:$LD_LIBRARY_PATH
            export PATH=/opt/Qt/${QT5}/gcc_64/bin:$PATH
            bit32=1
            break;
            ;;
        "Qt${QT5} 32-bit")
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/${QT5}/gcc_32/bin/qmake"
            build_executable="build-executable5"
            export LD_LIBRARY_PATH=/opt/Qt/${QT5}/gcc_32/lib:$LD_LIBRARY_PATH
            export PATH=/opt/Qt/${QT5}/gcc_32/bin:$PATH
            bit32=0
            break;
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

        echo -----------------------------------------------------------
		PS3='Please enter your choice: '
		options=("Debug" "Release" "Quit")
		select opt in "${options[@]}"
do
    case $opt in
        "Debug")
            echo "You chose choice $REPLY which is $opt"
			mkdir build
			cd build
			$qmakePath -project ../code/${PROGNAME}.pro
			$qmakePath ../code/${PROGNAME}.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make qmake_all
			make -j$(nproc)
			make clean -j$(nproc)
			cd ..
			rm -r build
            break;
            ;;
        "Release")
            echo "You chose choice $REPLY which is $opt"
			mkdir build
			cd build
			$qmakePath -project ../code/${PROGNAME}.pro
			if [ "$bit32" > 0 ] ;
			then 
				$qmakePath ../code/${PROGNAME}.pro -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
			else
				$qmakePath ../code/${PROGNAME}.pro -spec linux-g++ CONFIG+=release CONFIG+=qml_release && /usr/bin/make qmake_all
			fi
			
			                          
			/usr/bin/make -j$(nproc)
			/usr/bin/make clean -j$(nproc)
			cd ..
			rm -r build
			#
						    echo -----------------------------------------------------------
							PS3='Please enter your choice: '
							options=("Copy to linuxdeploy" "Copy to AppDirQt5" "Quit")
							select opt in "${options[@]}"
					do
						case $opt in
							"Copy to linuxdeploy")
								sokvag=`pwd`
								cp -f $build_executable/testprg linuxdeploy/
								break;
								;;
							"Copy to AppDirQt5")
								sokvag=`pwd`
								cp -f $build_executable/testprg AppDirQt5/usr/bin/
								break;
								;;
							"Quit")
								break
								;;
							*) echo "invalid option $REPLY";;
						esac
					done
		#
            break;
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done



# AppImage
        echo -----------------------------------------------------------
		PS3='Please enter your choice: '
		options=("Run this script again" "Build AppImage" "Do not build AppImage" "Quit")
		select opt in "${options[@]}"
do
    case $opt in
            "Run this script again")
            echo "You chose choice $REPLY which is $opt"
            ./`basename "$0"`
            break;
            ;;
        "Build AppImage")
            echo "You chose choice $REPLY which is $opt"
            ./build_appimage.sh
            break;
            ;;
        "Do not build AppImage")
            echo "You chose choice $REPLY which is $opt"
            echo "Goodbye"
	        exit 0
            break;
            ;;
        "Quit")
            break
            exit 0;
            ;;
        *) echo "invalid option $REPLY";;
    esac
done


