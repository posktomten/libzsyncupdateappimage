#!/bin/bash

# Edit
QT6=6.7.2
QT5=5.15.14
EXECUTABLE=testprg
EXTRA_LIB=./openssl

[ -e ${EXECUTABLE}-*.AppImage ] && rm ${EXECUTABLE}-*.AppImage
[ -e AppDir ] && rm -R AppDir


echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Qt${QT5} 64-bit" "Qt${QT5} 32-bit" "Qt${QT6}" "Quit")

select opt in "${options[@]}"; do
    case $opt in
    "Qt${QT5} 32-bit")
        export PATH=/opt/Qt/${QT5}/gcc_32/bin:$PATH
        export LD_LIBRARY_PATH=/opt/Qt/${QT5}/gcc_32/lib:${EXTRA_LIB}:$LD_LIBRARY_PATH
        APPIMAGE=i386.AppImage
        break
        ;;
    "Qt${QT5} 64-bit")
        export PATH=/opt/Qt/${QT5}/gcc_64/bin:$PATH
        export LD_LIBRARY_PATH=/opt/Qt/${QT5}/gcc_64/lib:${EXTRA_LIB}:$LD_LIBRARY_PATH
        APPIMAGE=x86_64.AppImage
        break
        ;;
    "Qt${QT6}")
        export PATH=/opt/Qt/${QT6}/gcc_64/bin:$PATH
        export LD_LIBRARY_PATH=/opt/Qt/$${EXECUTABLE}{QT6}/gcc_64/lib:${EXTRA_LIB}:$LD_LIBRARY_PATH
        APPIMAGE=x86_64.AppImage
        break
        ;;
    "Quit")
        echo "Goodbye!"
        exit 0
        break

        ;;
    *) echo "Invalid option $REPLY"
    ;;
    esac
done


linuxdeploy --appdir AppDir \
-e ./${EXECUTABLE} \
-d ./${EXECUTABLE}.desktop \
-e ./openssl/openssl \
-e ./zsync \
-l ./openssl/libssl.so \
-l ./openssl/libssl.so.3 \
-l ./openssl/libcrypto.so \
-l ./openssl/libcrypto.so.3 \
-i ./16x16/${EXECUTABLE}.png \
-i ./32x32/${EXECUTABLE}.png \
-i ./64x64/${EXECUTABLE}.png  \
-i ./128x128/${EXECUTABLE}.png \
-i ./256x256/${EXECUTABLE}.png \
-i ./512x512/${EXECUTABLE}.png \
--plugin qt

cp ./${EXECUTABLE}.png ./AppDir/usr/bin/
rm -R ./AppDir/usr/translations
appimagetool AppDir

echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Copy" "Upload" "Quit")
select opt in "${options[@]}"; do
    case $opt in
    "Copy")
        echo "You chose choice $REPLY which is $opt"
        yes | cp -rf ${EXECUTABLE}-${APPIMAGE} ../
        break
        ;;
    "Upload")
        echo "You chose choice $REPLY which is $opt"
        yes | cp -rf ${EXECUTABLE}-${APPIMAGE} ../
        cd ..
        ./upload_appimage.sh
        break
        ;;
    "Quit")
        break
        ;;
    *) echo "invalid option $REPLY" ;;
    esac
done
