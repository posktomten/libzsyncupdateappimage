/********************************************************************************
** Form generated from reading UI file 'updatedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UPDATEDIALOG_H
#define UI_UPDATEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_UpdateDialog
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QLabel *lblIcon;
    QLabel *lbl;

    void setupUi(QDialog *UpdateDialog)
    {
        if (UpdateDialog->objectName().isEmpty())
            UpdateDialog->setObjectName(QString::fromUtf8("UpdateDialog"));
        UpdateDialog->resize(400, 400);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(UpdateDialog->sizePolicy().hasHeightForWidth());
        UpdateDialog->setSizePolicy(sizePolicy);
        UpdateDialog->setStyleSheet(QString::fromUtf8("QDialog {border:1px solid black;background-color: rgb(218, 255, 221)}\n"
""));
        gridLayout = new QGridLayout(UpdateDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(-1, 18, -1, -1);
        lblIcon = new QLabel(UpdateDialog);
        lblIcon->setObjectName(QString::fromUtf8("lblIcon"));
        lblIcon->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(lblIcon);

        lbl = new QLabel(UpdateDialog);
        lbl->setObjectName(QString::fromUtf8("lbl"));
        QFont font;
        font.setPointSize(24);
        lbl->setFont(font);
        lbl->setFrameShape(QFrame::NoFrame);

        verticalLayout->addWidget(lbl);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        retranslateUi(UpdateDialog);

        QMetaObject::connectSlotsByName(UpdateDialog);
    } // setupUi

    void retranslateUi(QDialog *UpdateDialog)
    {
        UpdateDialog->setWindowTitle(QCoreApplication::translate("UpdateDialog", "Dialog", nullptr));
        lblIcon->setText(QCoreApplication::translate("UpdateDialog", "TextLabel", nullptr));
        lbl->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class UpdateDialog: public Ui_UpdateDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UPDATEDIALOG_H
